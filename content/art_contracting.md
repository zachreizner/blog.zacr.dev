+++
title = "Which Kernel is this Board Using?"
date = 2020-10-06
[taxonomies]
tags = ["gamedev"]
categories = ["Titanean"]
+++

Z: As someone looking into getting contractors, I was wondering if you have any pointers to resources or general tips about getting started with that? I've noticed you use Fiverr sometimes too.

A: oh ho ho ho! Fiverr **SOMETIMES!??!** 
broooo
hahaha
I've placed like 120 orders on Fiverr (just placed another last week and giving feedback on 2 others today).

Z:
So you use them exclusively? Seems like it must be going well?

A:
Primarily Fiverr yeah, There's like a learning curve to scouting/finding talent. So at first I had like maybe a 50% success rate of hiring folks and getting the quality work I was looking for, but then once you find like 1-2 folks who do what you're looking for, you just repeat buy from them and its great and you eventually get better and evaluating talent based on their portfolios.
But yeah, to date I've looked for contractors:

- Fiverr
- Reddit (gamedevclassifieds, starvingartists i think 1 more?)
- Twitter
- Upwork (very briefly)
- Artstation

Generally it's much better to use an established marketplace (Fiverr, Upwork, Artstation) than individual's (Reddit, Twitter). Even if they say "commissions open" those folks are usually much less professional or reliable. They're cool for like 1-offs. They're more like enthusiasts who happen take a few work requests.

Z:
Interesting, thanks for the tips. It sounds like it's not worth looking outside those marketplaces you mentioned. How do you feel about Artstation?

A
Arstation is where the super pros are. It's nuts. They're extremely talented. They're just less available since many are like working full time as artists, at like actual dev studios, but man, they're skilled AF. They tend to be more expensive than Fiverr and also you have to manage the whole transaction yourself (paypal, tell them where to upload the files, follow up if they're taking long to deliver). Whereas a marketplace does all that for you, but the quality of work, I've found overall, to be consistently high.

Z:
I see the tradeoffs. And how do you tend to evaluate talent?

A:
Granted, I've placed maybe 6-10 orders on Artstation by comparison to the 120+ on Fiverr, but those first 6-10 were much better than the first 6-10 i placed on Fiverr. I evaluate them in 2 steps:

1) look at their portfolio and see if they have clear examples of the art style or "look" that I'm going for, **OR** show a variety of styles which indicates adaptability. Many people say, "I can adapt!" but sadly they can'. Folks have their strengths.

2) I place a small sample order as a final check. Something they can do briefly that costs $5-$20 (sometimes $50 depending). That's the real measure
once you get a piece of work from them of exactly what you're looking for, evaluating becomes a breeze.

for example these samples:

for this character

Each order cost me like $50 i think, but, much less $$ and less stress than placing a full order with either guy just to find out they couldn't quite nail the look I was going for.

Z:
Your advice makes a lot of sense.
However, clicking around Artstation, the only Marketplace I see is for pre-made assets. Is there something I'm missing for doing commissions?

A:
Oh right, yeah, so Artstation since its not primarily a marketplace, you have to contact the artists yourself through a DM on the site. They usually list who's open to freelance or contract work on their profiles. Head here: https://www.artstation.com/search/artists. Then filter for what your looking for (environment art, character modeling, concept art, etc.).

Z:
Do you use "Pro Members First" in your search?

A:
Then you'll get a big list, the top results are for "pro" members who usually charge more, so because I'm budget limited, I usually skip them or filter them out through search results.

What work are you looking to commission exactly?
I usually recommend people start on Fiverr so yo can make your mistakes in learning the ropes in a low-stakes and easy to use platform. The direct contact route isn't really great unless you:

A) know exactly what you're looking for and how to ask for it and 
B) are familiar with going rates for that work

Z:
Pixel art animations both characters and objects.

A:
Ok cool. are the characters and objects art already defined? Do you know what FPS you want to target? I think pixel art you can find a lot of sellers on Fiverr for. I'd recommend starting there and after working w/ like 3-5 of them, head over to Artstation.

Z:
Not exactly known. I mean, some objects are known, but not like the entire game's worth of assets.

A:
its hard to explain in text, but, you end up learning a lot more about how to do contracting better by doing it, and you don't want to lose a potential artist contact due to inexperience. I'm probably butchering the explanation but its like: you're gonna make mistakes and you're gonna learn more about what you need exactly. So it helps a **lot** to do that in an easier, low stakes environment.
