+++
title = "Which Kernel is this Board Using?"
date = 2020-07-08
[taxonomies]
tags = ["linux", "cros"]
categories = ["Chrome OS"]
+++

A typical question I find myself asking over the course of my work day is,
"Which is kernel is a given board using?" It's a fundamental question that is
complicated by the system of overlays, baseboards, and chipsets that exist in
Chrome OS. To find the answer, let's go directly to the [source].

Using the `eve` board (Google Pixelbook) as an example, we would want to dive
into the `overlay-eve` directory. Actually, we need to dive really deep, all
the way to [`overlay-eve/profiles/base/make.defaults`]. What we're looking at
here is a long series of modifications to the `USE` flags of this particular
board. If we were lucky (we're not) we would see a line like this:

```bash
USE="${USE} kernel-4_4"
```

This of course indicates the 4.4 kernel[^kversions] is being used.

However, `eve` and quite a few other board will have no such line, which means
we have to look at the parent, which is identified by a sibling file to
`make.defaults` called [`parent`]. For `eve` we see there is this line:

```
chipset-kbl:base
```

This indicates that some `USE` modifications were inherited from the
`chipset-kbl`[^kbl] overlay. Just like with `eve`, we will dive into the
[`make.defaults` of `chipset-kbl`] in search of the `USE` modification that
includes a kernel version, and this we come up with version 4.4. Easy! In your
particular board, there may be more layers of inheritance, but the basic
algorithm remains the same:

1. Look at `make.defaults` of `overlay-${BOARD}` for a line with `kernel-` and
read the version.

1. If there is no such line, look for a `parent` and use that to restart the
search of a kernel

### Bonus Section

If you're interested in the nuts and bolts of how this work, you should learn
about [`USE` flags] in the portage build system. For every build of Chrome OS,
the `linux-sources` package is depended on, which will reference the present
`USE` flag to choose an appropriate kernel package.[^lsource] As far as I
know, this is the only Chrome OS package that works this way rather than just
having multiple versioned ebuilds for a singular package. The reason being
that the kernel is the only thing we would want to maintain multiple source
repositories of concurrently. The reason for that could probably take an blog
post.

With this knowledge, we actually have a short cut for determining the kernel
version, assuming the `${BOARD}` has been setup and built at least once in our
active `cros_sdk`.

```bash
equery-${BOARD} list 'chromeos-kernel-*'
```

Output for `BOARD=eve`:

```
 * Searching for chromeos-kernel-* ...
[I-O] [  ] sys-kernel/chromeos-kernel-4_4-4.4.229-r2297:0
```

That confirms what we already know: kernel version 4.4. I could go on but
hopefully you get the idea. As an exercise for the reader, try determining the
`USE` flags that `linux-sources` would have for a given board.[^equery]

---

[^kversions]: As of 2020-07-08, possible Chrome OS kernel versions are 3.8,
3.10, 3.14, 3.18, 4.4, 4.14, 4.19, or 5.4. See [this
directory](https://source.chromium.org/chromiumos/chromiumos/codesearch/+/master:src/third_party/kernel/)
for up to date info.

[^kbl]: Short for [Kaby Lake](https://en.wikipedia.org/wiki/Kaby_Lake)

[^lsource]: The [linux-sources ebuild](https://source.chromium.org/chromiumos/chromiumos/codesearch/+/master:src/third_party/chromiumos-overlay/virtual/linux-sources/linux-sources-1.ebuild) for the curious.

[^equery]: `equery-${BOARD} uses linux-sources`

[source]: https://source.chromium.org/chromiumos/chromiumos/codesearch/+/master:src/overlays/

[`overlay-eve/profiles/base/make.defaults`]: https://source.chromium.org/chromiumos/chromiumos/codesearch/+/master:src/overlays/overlay-eve/profiles/base/make.defaults

[`parent`]: https://source.chromium.org/chromiumos/chromiumos/codesearch/+/master:src/overlays/overlay-eve/profiles/base/parent

[`make.defaults` of `chipset-kbl`]: https://source.chromium.org/chromiumos/chromiumos/codesearch/+/master:src/overlays/chipset-kbl/profiles/base/make.defaults

[`USE` flags]: https://wiki.gentoo.org/wiki/USE_flag