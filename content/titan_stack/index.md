+++
title = "Tech Stack"
date = 2020-07-12
[taxonomies]
tags = ["rust", "gamedev"]
categories = ["Titanean"]
+++

The itch to make a new game, with accompanying engine is back, and with a
vengeance. This game will be an immersive simulation of the 0451[^0451]
variety. Because of artistic, time, and budgetary reasons, it will be a side
view 2D adventure, which would be a significant departure for the formula.

To summarize here it was I will be using:

- Programming Language: [Rust](https://rust-lang.org/)
- Graphics: [wgpu-rs]
- Physics: [nphysics](http://nphysics.org/)
- Entity-Component System: [specs](https://github.com/amethyst/specs)

### Rust

I have been a long time user of Rust (back when it had sygils for GC), and I
use it everyday. I've used C++ for previous games and it is the traditional
game dev language, but I think Rust's ecosystem has matured enough to make it
viable. I vastly prefer using Rust for systems programming, which I consider
game development a form of.

Rust is also a great language for WebAssembly, which means that Titanean could
potentially run in the browser. All the main dependencies are compatible with
this target, while also being of high quality.

### wgpu-rs

The [wgpu-rs] is the Rust bindings for [wgpu], itself an implementation of
WebGPU on top of [gfx-rs] on top of native graphics APIs.[^apis] The nice
thing about wgpu is that it may utilize "native" WebGPU when compiled to
WebAssembly. Currently, WebGPU isn't stable[^wgsl] or included in any web
browser's stable release, but it can be tested in Chrome Canary for
Windows/Mac and Firefox Nightly.[^impl] The other really nice thing about wgpu
is that it is a very simplified form of the new generation of low level
graphics APIs. There is no need to bring your own memory allocator or deal
with heaps or resource transitions. The code to upload a new uniform buffer
couldn't be easier:

```rust
fn upload_uniform_matrix(
    device: &wgpu::Device,
    matrix: &[f32],
) -> wgpu::Buffer {
    device.create_buffer_with_data(
        unsafe { slice_as_bytes(&matrix) }, // Forgive the unsafe :(
        wgpu::BufferUsage::COPY_SRC,
    )
}
```

Because WebGPU is designed to be safe for the open web, it fits in great with
Rust's safety efforts, while the others are hard to reconcile due to their raw
nature. I appreciate that especially because of my limited personal experience
with Vulkan, and none of the other APIs. The portability will also prove
useful if this ever reaches a releasable state.

### nphysics

I should mention I'm not just using nphysics, but also the associated crates
under the [rustsim] project. That means I will also utilize [ncollide] and
[nalgebra]. As a perk, nalgebra will also do all the fun vector math that is
so critical to computer graphics. Otherwise, I might have had to include some
other crate, such as [cgmath], but I hear that usage share of that crate is
shifting to nalgebra anyhow.

As far as Rust based game physics engines, this collection is the only that
I'm aware of. There are others which are bindings to the excellent [Box2D]
library, which I've used in the past. However, that might make WebAssembly
usage complicated, in contrast to nphysics's explicit support for WebAssembly.

### specs

Specs is a crate for [ECS] that I've used before and it has worked to my
satisfaction, although it can be hard to reason about because of its extensive
use of generics. There canonical example of a system illustrates this:

```rust
struct Sys;

impl<'a> System<'a> for Sys {
    type SystemData = (WriteStorage<'a, Pos>, ReadStorage<'a, Vel>);

    fn run(&mut self, (pos, vel): Self::SystemData) {
        /* ... */
    }
}
```

That `SystemData` associated type is doing the heavy lifting. It can sometimes
feel like the hardest part about writing a system, but I find it worth it.

By the nature of ECSs, any particular system can be harder to write than if it
were simply coded directly into the game loop, but the trade off is that I
find the game code much more organized and easy to reason about as it gets
quite large. It forces me to think about everything with a consistent mental
framework. ECS is particularly well suited for simulations and immersive
simulations are all about that.

### Current Status

There are a fluctuating set of miscellaneous other crates and
sub-dependencies. Currently that weighs in at **253** total crates, which
takes \~45 seconds to build from scratch in debug mode. In release mode, the
build time goes up only mildly to \~75 seconds. The debug build's binary is
117MB versus the release build's 7.9MB. This is all compiled with rustc 1.44.0
on my Arch Linux machine with AMD Ryzen 2700x, Samsung NVMe Drive, and 16GB
RAM.

In the interest of showing our humble beginning, this is what the game
engine's output currently looks like:

{{ image_zoom(src="screen.png", title="Screenshot of game engine with imgui showing debug info about the loader, a frame time display, a circle, a line, and a solid dusty blue background.") }}

It renders all that in under a millisecond on my AMD Vega 64!

---

[^0451]: This sub-genre of video games was pioneered by System Shock and its
spiritual successors such as Bioshock and Prey. [See this great Errant Signal
episode](https://www.youtube.com/watch?v=zAtAyycx-uY).

[^apis]: Metal, DX12, Vulkan, and OpenGL

[^wgsl]: Even the choice of shading language is in flux, as Apple rejected
SPIR-V and the consensus seems to be to invest [yet another shading
language](https://gpuweb.github.io/gpuweb/wgsl.html).

[^impl]: [WebGPU implementation status](https://github.com/gpuweb/gpuweb/wiki/Implementation-Status)

[wgpu-rs]: https://github.com/gfx-rs/wgpu-rs
[wgpu]: https://github.com/gfx-rs/wgpu
[gfx-rs]: https://github.com/gfx-rs/gfx
[rustsim]: https://github.com/rustsim
[nphysics]: https://github.com/rustsim/nphysics
[ncollide]: https://github.com/rustsim/ncollide
[nalgebra]: https://github.com/rustsim/nalgebra
[cgmath]: https://github.com/rustgd/cgmath
[Box2D]: https://box2d.org/
[ECS]: https://gameprogrammingpatterns.com/component.html