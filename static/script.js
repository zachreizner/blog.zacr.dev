const onLoad = () => {
    for (let e of document.getElementsByClassName('footnote-definition')) {
        const link = document.querySelectorAll(`[href="#${e.id}"]`)[0];

        // Add a hover popup with the footnote content.
        const popup = document.createElement("div");
        const popupContent = e.getElementsByTagName("p")[0].cloneNode(true);
        popupContent.style.lineHeight = "1.5em";
        popupContent.style.padding = "4px";
        popupContent.style.margin = "0";
        popup.appendChild(popupContent);
        document.body.appendChild(popup);
        popup.style.position = "absolute";
        popup.style.maxWidth = "400px";
        popup.style.backgroundColor = "white";
        popup.style.border = "solid #777 1px";
        popup.style.borderRadius = "4px";
        popup.style.boxShadow = "0 0 5px black";
        popup.style.whiteSpace = "wrap";
        popup.hidden = true;
        // The ups variable is used as a counter for the number of things that
        // want the popup to be up.
        let ups = 0;
        const startPopup = () => {
            ups += 1;
            popup.hidden = false;
            popup.style.left = Math.max(0, link.offsetLeft - popup.offsetWidth / 2) + "px";
            popup.style.top = (link.offsetTop - popup.offsetHeight) + "px";
        };
        const endPopup = () => {
            ups -= 1;
            popup.hidden = ups === 0;
        };
        link.addEventListener("mouseenter", startPopup);
        link.addEventListener("mouseleave", endPopup);
        popup.addEventListener("mouseenter", startPopup);
        popup.addEventListener("mouseleave", endPopup);

        // Add an id to each footnote link so that it may be jumped to from
        // the footnotes.
        const linkId = `${e.id}-link`;
        link.setAttribute("id", linkId)
        // Add a jump link to each footnote to find it in the content.
        const jumpLink = document.createElement("a");
        jumpLink.innerText = "^";
        jumpLink.setAttribute("href", `#${linkId}`);
        e.getElementsByTagName("sup")[0].appendChild(jumpLink);
    }
}

if (document.readyState !== "loading") {
    onLoad();
} else {
    document.addEventListener('DOMContentLoaded', () => onLoad());
}